# Contributor: Siva Mahadevan <me@svmhdvn.name>
# Contributor: Simon Frankenberger <simon-alpine@fraho.eu>
# Maintainer: Simon Frankenberger <simon-alpine@fraho.eu>
pkgname=signal-cli
pkgver=0.11.2
pkgrel=0
pkgdesc="commandline interface for libsignal-service-java"
url="https://github.com/AsamK/signal-cli"
# jdk17 only available on 64 bit archs
# cargo and rust not available on s390x and riscv64
# java-libsignal-client not available on aarch64
arch="x86_64 ppc64le"
license="GPL-3.0-or-later"
depends="java-libsignal-client"
makedepends="openjdk17-jdk"
source="$pkgname-$pkgver.tar.gz::https://github.com/AsamK/signal-cli/archive/v$pkgver.tar.gz"

build() {
	./gradlew installDist
}

check() {
	./gradlew check
}

package() {
	local buildhome="build/install/$pkgname"
	local installhome="/usr/share/java/$pkgname"

	install -dm755 "$pkgdir/$installhome/lib"
	install -m644 "$buildhome"/lib/* "$pkgdir/$installhome/lib"

	install -Dm755 "$buildhome/bin/$pkgname" "$pkgdir/$installhome/bin/$pkgname"
	install -dm755 "$pkgdir/usr/bin"
	ln -s "$installhome/bin/$pkgname" "$pkgdir/usr/bin/$pkgname"

	rm -v "$pkgdir/$installhome/lib"/libsignal-client-*.jar
	ln -sv /usr/share/java/libsignal-client/signal-client-java.jar "$pkgdir/$installhome/lib/signal-client-java.jar"
	sed -i -e 's/libsignal-client-[0-9.]\+\.jar/signal-client-java.jar/g' "$pkgdir/$installhome/bin/$pkgname"
}

sha512sums="
9d9191e6b244b80555f4479bff3d1a7aae24153f16423dc43895f89988f91a00b41ebb927a6d67c1fbd208f18c73325c253074a426210d50a8fc4b8962d93579  signal-cli-0.11.2.tar.gz
"
